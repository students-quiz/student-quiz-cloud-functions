/* eslint-disable max-len */
const functions = require("firebase-functions");
const admin = require("firebase-admin");
exports.score = require("./score");
exports.questionutil = require("./question-util");
const defaultTags = ["General Knowledge"];

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });


admin.initializeApp();

exports.questionOnCreate =
    functions.firestore
        .document("/questions/{documentId}")
        .onCreate((snap, context) => {
            return onCreate(snap, context);
        });
const onCreate = async (snap, context) => {
    const newValue = await addDefaultTagIfNoTag(snap);
    const keywords = await generateKeyWord(newValue);
    saveKeys(context, keywords);
    generateNewLeafTags(newValue.tags, []);
    deleteBonusQFromHist(newValue.bonusParent);
};
/**
 * if bonusParent present delete the parent from all users history,
 * to be treated as new question
 * @param {String} bonusParent
 */
const deleteBonusQFromHist = async (bonusParent) => {
    if (bonusParent) {
        const batch = admin.firestore().batch();
        const docSnap = await admin.firestore()
            .collection("questions")
            .doc(bonusParent)
            .get();
        const question = docSnap.data();
        const qSnap = await admin.firestore()
            .collection("users")
            .get();
        for (let i =0; i< qSnap.docs.length; i++) {
            // for loop used instead of foreach to use await/async without inner function, which creates problem
            const d = qSnap.docs[i];
            batch.delete(d.ref.collection("q_history").doc(bonusParent));
            const user = d.data();
            const docSnap = await d.ref.collection("q_history").doc(bonusParent).get();

            if (user.tagscount && docSnap.exists) {
                // if the bonusPaent wxist in the history only then decrese tags count
                // other wise if two more than one bonus added for the same question then
                // it will decrease the count more than once
                const tagsC = user.tagscount;
                question.tags.forEach((t) => {
                    if (tagsC[t]) {
                        tagsC[t] = tagsC[t] - 1;
                    }
                });
                batch.set(d.ref,
                    {tagscount: tagsC},
                    {merge: true});
            }
        }

        await batch.commit();
    }
};

exports.questionOnUpdate = functions.firestore.document("/questions/{documentId}").onUpdate((snap, context) => {
    return onUpdate(snap, context);
});

const onUpdate = async (snap, context) => {
    generateKeyWordsOnUpdate(snap, context);
    generateNewTagsOnUpdate(snap, context);
};


const generateKeyWordsOnUpdate = async (snap, context) => {
    functions.logger.log("Generating keyword for : ", context.params.documentId);
    const newValue = snap.after.data();
    const oldValue = snap.before.data();
    const keywords = [];
    if (newValue.question != oldValue.question) {
        return generateKeyWord(newValue)
            .then((keywords) => {
                saveKeys(context, keywords);
            });
    }
    return admin.firestore().collection("keywords").doc(context.params.documentId + "_keys").set({
        keywords,
    });
};

const addDefaultTagIfNoTag = async (after) => {
    const newValue = after.data();
    if (!Array.isArray(newValue.tags) || (newValue.tags.length === 0)) {
        newValue.tags = defaultTags;
        await after.ref.set({
            tags: defaultTags,
        }, {merge: true});
    }
    return newValue;
};

const generateNewTagsOnUpdate = async (snap, context) => {
    functions.logger.log("Generating new tags for : ", context.params.documentId);
    const newValue = await addDefaultTagIfNoTag(snap.after);
    const oldValue = snap.before.data();
    if (!arrayEquals(newValue.tags, oldValue.tags)) {
        return generateNewLeafTags(newValue.tags, oldValue.tags);
    }
};

const arrayEquals = (a, b) => {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
};

const arrayDiff = (a, b) => {
    let plus = [];
    let minus = [];
    if (a && b) {
        a.forEach((val) => {
            if (!b.includes(val)) {
                plus.push(val);
            }
        });
        b.forEach((val) => {
            if (!a.includes(val)) {
                minus.push(val);
            }
        });
    } else if (a && !b) {
        plus = a;
    } else if (!a && b) {
        minus = b;
    }
    return {
        plus: plus,
        minus: minus,
    };
};

const generateNewLeafTags = async (newtags, oldtags) => {
    const querySnapshot = await admin.firestore().collection("tags")
        .where("display_name", "in", newtags)
        .get();
    const savedTagNames = [];
    const savedTags = [];
    querySnapshot.forEach((documentSnapshot) => {
        savedTagNames.push(documentSnapshot.data().display_name);
        savedTags.push(documentSnapshot.data());
    });

    const bulkWriter = admin.firestore().bulkWriter();
    const collectionRef = admin.firestore().collection("tags");
    const newTags = [];
    let needWrite = false;

    const tagDiff = arrayDiff(newtags, oldtags);
    const decrement = admin.firestore.FieldValue.increment(-1);
    const increment = admin.firestore.FieldValue.increment(1);
    newtags.forEach((t) => {
        if (!savedTagNames.includes(t)) {
            needWrite = true;
            newTags.push(t);
            const newTag = {
                display_name: t,
                leaf: true,
                count: 1,
            };
            bulkWriter.create(collectionRef.doc(getTagkey(t)), newTag);
        } else if (tagDiff.plus.includes(t)) {
            needWrite = true;
            bulkWriter.update(collectionRef.doc(getTagkey(t)), {count: increment});
        }
    });
    tagDiff.minus.forEach((t) => {
        needWrite = true;
        bulkWriter.update(collectionRef.doc(getTagkey(t)), {count: decrement});
    });

    if (needWrite) {
        await bulkWriter.close().then(() =>
            functions.logger.log("Executed all writes : ", newTags),
        );
    }
};

const getTagkey = (tag) => {
    tag = tag.toLocaleLowerCase();
    tag = tag.replaceAll(" ", "_");
    return tag;
};

const generateKeyWord = async (data) => {
    const keys = [];
    const exclusionsData = await getExWords();
    const exclusions = exclusionsData.generatekeyword;
    if (data.question) {
        let q = data.question;
        exclusions.operators.forEach((element) => {
            q = q.replaceAll(element, "");
        });
        const spls = q.split(" ");
        for (let i = 0; i < spls.length; i++) {
            const s = spls[i];
            if (s) {
                if (!(exclusions.excludedwords.includes(s.toLocaleLowerCase()))) {
                    keys.push(s.toLocaleLowerCase());
                }
            }
        }
    }

    return keys;
};

const getExWords = async () => {
    const snap = await admin.firestore().collection("cloudfunctions-config").doc("cfg").get();
    return snap.data();
};
const saveKeys = (context, keywords) => {
    admin.firestore().collection("keywords").doc(context.params.documentId + "_keys").set({
        qid: context.params.documentId,
        keywords,
    });
};

