/* eslint-disable valid-jsdoc */
/* eslint-disable object-curly-spacing */

const functions = require("firebase-functions");
const admin = require("firebase-admin");

const mathjs = require("mathjs");

const config = {};
const math = mathjs.create(mathjs.all, config);

exports.calcScore = functions.https.onCall((data, context) => {
    return calculateScore(data, context)
        .then((score) => {
            return score;
        });
});

exports.activateBuff = functions.https.onCall((data, context) => {
    const userUid = context.auth.uid;
    return getBuff(data.buffName)
        .then((buff) => addBuffToUser(buff.docs[0].data(), userUid))
        .then((message) => {
            return message;
        })
        .catch((e) => {
            throw e;
        });
});

const getBuff = (buffName) => {
    return admin.firestore()
        .collection("buffs")
        .where("name", "==", buffName).get();
};

const addBuffToUser = (buff, userUid) => {
    return userDoc(userUid).get()
        .then((doc) => {
            const user = doc.data();
            if (user.buffs[buff.name] && user.buffs[buff.name] > 0) {
                user.buffs[buff.name]--;
                const currentEpoch = new Date().getTime();
                const expiryEpoch =
                    currentEpoch + buff.duration_hrs * 60 * 60 * 1000;
                user.activeBuff = {
                    "buff": buff.name,
                    "expireAt": expiryEpoch,
                };
                return userDoc(userUid).set(user);
            } else {
                throw new functions.https.HttpsError("invalid-argument",
                    "No Such buff");
            }
        })
        .then(() => {
            return "Success";
        });
};

const calculateScore = async (data, context) => {
    const userUid = context.auth.uid;
    functions.logger.log("Calculating score for  : ", userUid);
    const qHist = await getQhistory(data.questions, userUid);
    const correctAnswer = getCorrectAns(qHist, data);
    const actBuff = await getActiveBuff(userUid);
    const globalBuff = await getGlobalBuff();
    const reward = await getRewardOrBuff("rewards_probability", actBuff);
    const buff = await getRewardOrBuff("buffs_probability");
    const bonus = await getBonusReward(data.questions, qHist);

    let amount = 0.0;
    const rewardSnap = await admin.firestore().collection("rewards")
        .where("name", "==", reward)
        .get();
    let rw = null;
    rewardSnap.forEach((s) => {
        rw = s.data();
    });
    if (rw) {
        let weight = rw.weight;
        if (actBuff.effects == "amount" && actBuff.reward == rw.name) {
            const f = math.parse(actBuff.formula);
            weight = f.evaluate({ amount: weight });
        }
        weight = await applyGlobalBuff(globalBuff, rw, data.questions, weight);
        amount = correctAnswer * weight;
        // amount fixed to two decimal
        amount = parseFloat(amount.toFixed(2));
    }
    // save score/hist must be awaited otherwise its not getting saved
    await saveRewardAndBuff(reward, amount, buff, bonus, userUid);
    await saveQHist(qHist, userUid);
    return {
        "correctAnswer": correctAnswer,
        "reward": reward,
        "reward_amount": amount,
        "buff": buff,
        "bonus": bonus,
    };
};

const getBonusReward = async (questions, qHist) => {
    let count = 0.0;
    questions.filter((q) => q.bonus && q.correct)
        .forEach((q) => {
            // if previously answered devide the count
            // by the number of times previously answered
            count += 1 / (qHist[q.qid]);
        });
    let reward = "";
    let amount = 0;
    if (count > 0) {
        const weekDay = new Date().toLocaleString("en-us", { weekday: "long" });
        const bonusSettingsSnap = await admin.firestore()
            .collection("global_settings")
            .doc("bonus_reward_settings")
            .get();
        const bonusSettings = bonusSettingsSnap.data();
        if (weekDay in bonusSettings.weekday_reward) {
            // only if today has any reward
            reward = bonusSettings.weekday_reward[weekDay];
            amount = bonusSettings.reward_amount[reward] * count;
            // amount fixed to two decimal
            amount = parseFloat(amount.toFixed(2));
        }
    }

    return {
        "reward": reward,
        "amount": amount,
    };
};

const saveQHist = async (qHist, userUid) => {
    const batch = admin.firestore().batch();
    const newQs = [];
    Object.entries(qHist).forEach(([key, value]) => {
        // if count = 1 thatmeans this question newly attended
        (value == 1) && newQs.push(key);
        const ref = userDoc(userUid)
            .collection("q_history").doc(key);
        batch.set(ref,
            {
                count: value,
                timestamp: admin.firestore.FieldValue.serverTimestamp(),
            });
    });
    if (newQs.length > 0) {
        const userSnap = await userDoc(userUid).get();
        const user = userSnap.data();
        const tagsCount = user.tagscount ? user.tagscount : {};
        const qTags = await getQTags(newQs, tagsCount);
        user.tagscount = qTags;
        batch.set(userDoc(userUid), user);
    }
    await batch.commit();
};


const getQTags = async (qIds, tagsCount) => {
    // gets all questions with the qids, chunked to max size 10
    const snap = await getChunked(
        qIds,
        admin.firestore().collection("questions"),
        admin.firestore.FieldPath.documentId(),
    );

    snap.forEach((s) => {
        // creates tags count object {tag1 : <tag1Count>, tag2 : <tag2Count>}
        const q = s.data();
        if (q.tags) {
            q.tags.forEach((t) => {
                if (tagsCount[t]) {
                    tagsCount[t] = tagsCount[t] + 1;
                } else {
                    tagsCount[t] = 1;
                }
            });
        }
    });
    return tagsCount;
};

const getCorrectAns = (qHist, data) => {
    let correctAns = 0.0;
    data.questions.filter((q) => q.correct).forEach((q) => {
        correctAns += 1 / (qHist[q.qid]);
    });

    return correctAns;
};

const getQhistory = async (questions, userUid) => {
    const qData = {};
    const snaps = await getChunked(questions.map((q) => q.qid),
        userDoc(userUid).collection("q_history"),
        admin.firestore.FieldPath.documentId());
    questions.forEach((x) => qData[x.qid] = 1);
    snaps.forEach((q) => {
        qData[q.id] = q.data().count + 1;
    });

    return qData;
};
/**
 * get data chunked to max size 10
 * @param {Array} array
 * @param {FirebaseFirestore.DocumentReference.Collection} collection
 * @param {FieldPath or String} fieldpath
 * @returns {Snapshots}
 */
const getChunked = async (array, collection, fieldpath) => {
    const chunkSize = 10;
    const snaps = [];
    for (let i = 0; i < array.length; i += chunkSize) {
        const chunk = array.slice(i, i + chunkSize);
        const snap = await collection
            .where(fieldpath, "in", chunk)
            .get();
        snap.forEach((s) => snaps.push(s));
    }
    return snaps;
};

const saveRewardAndBuff = async (reward, rewardAmount,
    buff, bonus, userUid) => {
    const doc = await userDoc(userUid).get();
    const user = doc.data();
    let rewards = {};
    if (user.rewards) {
        rewards = user.rewards;
    } else {
        user.rewards = rewards;
    }
    if (rewards[reward]) {
        rewards[reward] = rewards[reward] + rewardAmount;
    } else {
        rewards[reward] = rewardAmount;
    }
    if (bonus.reward) {
        if (rewards[bonus.reward]) {
            rewards[bonus.reward] = rewards[bonus.reward] + bonus.amount;
        } else {
            rewards[bonus.reward] = bonus.amount;
        }
    }

    let buffs = {};
    if (user.buffs) {
        buffs = user.buffs;
    } else {
        user.buffs = buffs;
    }
    if (buffs[buff]) {
        buffs[buff] = buffs[buff] + 1;
    } else {
        buffs[buff] = 1;
    }

    await userDoc(userUid).set(user);
};

const chooseRewardBuff = (probabilities) => {
    let chosen = "";
    const chance = Math.random();
    let low = 0.0;
    let high = 0.0;
    for (const [key, value] of Object.entries(probabilities)) {
        high = low + value;
        if (chance >= low && chance < high) {
            chosen = key;
            break;
        }
        low = high;
    }
    return chosen;
};

const getRewardOrBuff = async (collection, actBuff = {}) => {
    const probSnap = await admin
        .firestore()
        .collection(collection).get();
    let probability = {};
    probSnap.forEach((snap) => {
        probability = snap.data().probabilities;
    });
    probability = buffProbability(probability, actBuff);
    const reward = chooseRewardBuff(probability);
    return reward;
};

const buffProbability = (probability, actBuff) => {
    if (actBuff.reward && probability[actBuff.reward] &&
        actBuff.effects == "chance") {
        const f = math.parse(actBuff.formula);
        const prob = probability[actBuff.reward];
        const calProb = f.evaluate({ chance: prob });
        const finalProb = {};
        Object.entries(probability).forEach(([k, v]) => {
            if (k == actBuff.reward) {
                finalProb[actBuff.reward] = calProb;
            } else {
                finalProb[k] = v * (1 - calProb) / (1 - prob);
            }
        });
        return finalProb;
    } else {
        return probability;
    }
};

const getGlobalBuff = async () => {
    const buffSnap = await admin.firestore()
        .collection("global_settings")
        .doc("active_global_buffs")
        .get();

    const buff = buffSnap.data().buff;
    const start = buff.start;
    const end = buff.end;

    if (admin.firestore.Timestamp.now() > start &&
        admin.firestore.Timestamp.now() < end) {
        const bf = await getBuff(buff.name);
        return bf.docs[0].data();
    } else {
        return {};
    }
};

const getActiveBuff = async (userUid) => {
    const user = await userDoc(userUid).get();
    const activeBuff = user.data().activeBuff;
    if (activeBuff) {
        const expEpoch = activeBuff.expireAt;
        const currentEpoch = new Date().getTime();
        if (currentEpoch < expEpoch) {
            const buff = await getBuff(activeBuff.buff);
            return buff.docs[0].data();
        } else {
            return {};
        }
    } else {
        return {};
    }
};

const userDoc = (userUid) => {
    const userRef = admin.firestore()
        .collection("users");
    return userRef.doc(userUid);
};

const applyGlobalBuff = async (globalBuff, reward, questions, weight) => {
    if (globalBuff.effects == "amount" &&
        globalBuff.reward == reward.name) {
        const corrects = questions.filter((q) => q.correct);
        const correctQid = corrects.map((q) => q.qid);
        let factor = 1.0;
        if (Array.isArray(globalBuff.tags)) {
            const qs = await getQuestions(correctQid, globalBuff.tags);
            if (qs.length > 0) {
                factor = qs.length / questions.length;
            } else {
                return weight;
            }
        }
        const f = math.parse(globalBuff.formula);
        weight = factor * f.evaluate({ amount: weight });
    }
    return weight;
};

const getQuestions = async (qIds, tags) => {
    const snap = await admin.firestore()
        .collection("questions")
        .where(admin.firestore.FieldPath.documentId(),
            "in", qIds)
        .get();

    const qs = [];
    snap.forEach((s) => {
        const q = s.data();
        const found = tags.some((t) => q.tags.includes(t));
        if (found) {
            qs.push(s.data());
        }
    });
    return qs;
};

