const functions = require("firebase-functions");
const admin = require("firebase-admin");

exports.updateQuestionCount = functions.https.onCall((data, context) => {
    return admin.firestore()
        .collection("tags")
        .get()
        .then(async (snap) => {
            const zeroQuestionTags = [];
            for (const doc of snap.docs) {
                const size =
                    await countQuestionForTag(doc.id, doc.data().display_name);
                if (size == 0) {
                    zeroQuestionTags.push(doc.id);
                }
            }
            return {
                "zeroQuestionTags": zeroQuestionTags,
            };
        });
});
/**
 * Loops through all users
 * goes through all the questions user has attended in the past
 * updates/adds the count of tags user has attended
 */
exports.updateUserQuestionCount = functions.https.onCall((data, context) => {
    return admin.firestore()
        .collection("users")
        .get()
        .then(async (snap) => {
            for (const doc of snap.docs) {// loop through users
                const snap = await doc.ref.collection("q_history").get();
                const qIds = [];
                snap.forEach((s) => {
                    qIds.push(s.id);
                });
                const QTags = await getQTags(qIds);
                await doc.ref.set(// set tagsCount to user
                    {tagscount: QTags},
                    {merge: true},
                );
            }
        });
});

const getQTags = async (qIds) => {
    const chunkSize = 10;
    const tagsCount = {};
    for (let i = 0; i < qIds.length; i += chunkSize) {
        const chunk = qIds.slice(i, i + chunkSize);
        const snap = await admin.firestore() // gets all questions with the qids
            .collection("questions")
            .where(admin.firestore.FieldPath.documentId(),
                "in", chunk)
            .get();
        snap.forEach((s) => {
            // creates tags count object
            // {tag1 : <tag1Count>, tag2 : <tag2Count>}
            const q = s.data();
            if (q.tags) {
                q.tags.forEach((t) => {
                    if (tagsCount[t]) {
                        tagsCount[t] = tagsCount[t] + 1;
                    } else {
                        tagsCount[t] = 1;
                    }
                });
            }
        });
    }

    return tagsCount;
};


const countQuestionForTag = (id, tag) => {
    return admin.firestore()
        .collection("questions")
        .where("tags", "array-contains", tag)
        .get()
        .then((snap) =>
            updateTagQuestionCount(id, snap.size)
                .then(() => {
                    return snap.size;
                }),
        );
};

const updateTagQuestionCount = async (id, count) => {
    const snap = await admin.firestore()
        .collection("tags")
        .doc(id)
        .get();
    const tag = snap.data();
    tag.count = count;
    await admin.firestore()
        .collection("tags")
        .doc(id)
        .set(tag);
};
